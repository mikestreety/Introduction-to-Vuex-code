const ScoreBoard = {
	name: 'ScoreBoard',

	props: {
		team: Object
	},

	template: `<div class="score">
		<h3>{{ team.score }}</h3>
		<button>+1 point</button>
	</div>`
};

const TeamCard = {
	name: 'TeamCard',

	components: {
		ScoreBoard
	},

	props: {
		team: Object
	},

	template: `<div class="team">
		<h2>{{ team.name }}</h2>
		<score-board :team="team">
	</div>`
};

const store = new Vuex.Store({

});

new Vue({
	el: '#app',
	store,

	components: {
		TeamCard
	},

	data: {
		teams: {
			a: {
				name: 'Team A',
				score: 2
			},
			b: {
				name: 'Team B',
				score: 2
			}
		}
	},

	computed: {
		headline() {
			let output = '',
				scores = Object.keys(this.teams).map(t => this.teams[t].score);

			if(scores[0] === scores[1]) {
				output = 'It is currently a draw!';

				if(scores[0] === 0) {
					output = 'No-one has scored yet';
				}
			} else {
				let team = this.teams.a;
				if(scores[0] < scores[1]) {
					team = this.teams.b;
				}

				output = `${team.name} is winning`;
			}

			return output;
		}
	}
});