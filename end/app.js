const ScoreBoard = {
	name: 'ScoreBoard',

	props: {
		teamId: String
	},

	computed: {
		team() {
			return this.$store.state.teams[this.teamId];
		}
	},

	methods: {
		incrementScore() {
			this.$store.commit('incrementPoint', this.teamId);
		}
	},

	template: `<div class="score">
		<h3>{{ team.score }}</h3>
		<button @click="incrementScore()">+1 point</button>
	</div>`
};

const TeamCard = {
	name: 'TeamCard',

	components: {
		ScoreBoard
	},

	props: {
		teamId: String
	},

	computed: {
		team() {
			return this.$store.state.teams[this.teamId];
		}
	},

	template: `<div class="team">
		<h2>{{ team.name }}</h2>
		<score-board :team-id="teamId">
	</div>`
};

const store = new Vuex.Store({
	state: {
		teams: {
			a: {
				name: 'Team A',
				score: 0
			},
			b: {
				name: 'Team B',
				score: 0
			}
		}
	},

	mutations: {
		incrementPoint(state, teamId) {
			state.teams[teamId].score++;
		},

		updateNames(state, payload) {
			state.teams.a.name = payload.a;
			state.teams.b.name = payload.b;
		},

		resetScores(state) {
			state.teams.a.score = state.teams.b.score = 0;
		}
	},

	actions: {
		resetTeamNames(store) {
			store.commit('updateNames', {
				a: 'Team A',
				b: 'Team B'
			});
		}
	},

	getters: {
		headline: state => {
			let output = '',
				teams = state.teams,
				scores = Object.keys(teams).map(t => teams[t].score);

			if(scores[0] === scores[1]) {
				output = 'It is currently a draw!';

				if(scores[0] === 0) {
					output = 'No-one has scored yet';
				}
			} else {
				let team = teams.a;
				if(scores[0] < scores[1]) {
					team = teams.b;
				}

				output = `${team.name} is winning`;
			}

			return output;
		}
	}
});

new Vue({
	el: '#app',
	store,

	components: {
		TeamCard
	},

	data: {
		teamNameA: '',
		teamNameB: ''
	},

	created() {
		this.resetNames();
	},

	methods: {
		updateTeamNames() {
			this.$store.commit('updateNames', {
				a: this.teamNameA,
				b: this.teamNameB
			});
		},

		resetNames() {
			this.$store.dispatch('resetTeamNames');

			this.teamNameA = this.$store.state.teams.a.name;
			this.teamNameB = this.$store.state.teams.b.name;
		},

		resetAll() {
			this.resetNames();
			this.$store.commit('resetScores');
		}
	}
});