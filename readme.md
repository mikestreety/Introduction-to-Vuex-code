# Implementing a Vuex Store  using Vue.js 

> Creating the state object, mutations, actions and getters

This repository follows the video tutorial and blog post found on my personal website.

- [Introduction to Vuex Part 1](https://www.mikestreety.co.uk/blog/introduction-to-vuex-managing-state-storage-and-sharing-data-between-components
) - managing state, storage and sharing data between components
- [Introduction to Vuex Part 2](https://www.mikestreety.co.uk/blogintroduction-to-vuex-implementation-part-2-video) - implementation (with screencast)
- [Vue: Using localStorage with Vuex store](https://www.mikestreety.co.uk/blog/vue-js-using-localstorage-with-the-vuex-store)
- [Vue Lifecycle](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)
